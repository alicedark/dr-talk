<footer class="footer">
    <!-- TODO: Add Link -->
    <div class="container px-4 py-3">
      <div class="row">

      <div class="col-lg-4 mb-5">
        <!-- <h5 class="footer-title"> -->
        <a href="index.php" class="w-100"><img src="assets/img/svg/footer_logo.svg" class="logo-img" alt="Dr.Talk Logo"></a>
      </div>

      <div class="col-lg-8">
        <div class="row">
          <div class="col-sm-6 mb-5 mb-sm-3">
            <h5 class="footer-title">聯繫我們</h5>

            <ul class="footer-ul-4-col">
              <li><a href="mailTo:dr.talk@dksh.com">dr.talk@dksh.com</a></li>
            </ul>
          </div>

          <div class="col-sm-6 mb-5 mb-sm-3">
            <h5 class="footer-title"><span>快速連結</span></h5>

            <ul class="footer-ul-4-col">
              <li><a href="about.php">關於我們</a></li>
              <li><a href="info.php">使用條款</a></li>
              <li><a href="privacy.php">隱私權政策</a></li>
              <li><a href="account.php">會員專區</a></li>
              <li><a href="news.php">最新消息</a></li>
              <li><a href="news.php">文章區</a></li>
              <li><a href="video-list.php">影片區</a></li>
              <li><a href="file-list.php">檔案區</a></li>
              <li><a href="contact.php">聯絡我們</a></li>
            </ul>
          </div>


          <div class="col-sm-6 mb-5 mb-sm-3">
            <h5 class="footer-title">追蹤我們</h5>

            <ul class="footer-ul-2-col">
              <li>
                  <a href="#">
                    <img src="assets/img/svg/footer_line_icon.svg" style="width: 1.5rem;" alt="Follow Us in Line">
                    <!-- <i class="fa-lg fab fa-line text-brand-line"></i> -->
                  </a>
              </li>
            </ul>
          </div>
        </div>
      </div>

      </div>
    </div>
  <p class="copyright">Copyright © Dr.Talk All rights reserved. Codepulse-網站架設</p>
  </footer>

<a class="scrollToTop" href="#"><i class="fas fa-chevron-up"></i></a>
<script src="assets/js/jquery-3.4.1.min.js"></script>
<script src="assets/js/popper-1.14.3.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/select2-4.1.0.min.js"></script>
<script src="assets/js/jquery.slick-1.9.0.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.magnificpopup-1.1.0.min.js"></script>
<script src="assets/js/common.js"></script>
<script>
document.addEventListener("DOMContentLoaded", function() {
  var lazyImages = [].slice.call(document.querySelectorAll(".lazy"));

  if ("IntersectionObserver" in window) {
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          lazyImage.classList.remove("lazy");
          lazyImageObserver.unobserve(lazyImage);
            console.log(lazyImage.id);
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  } else {
  }
});

</script>
	</body>
</html>