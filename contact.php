<?php include('header.php'); ?>
<div class="pagecont border-top">
  <!-- 1920 * 500 -->
  <div class="banner banner-page" style="background-image: url('assets/img/png/banner_img.png');"></div>

  <div class="container pt-3">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
        <li class="breadcrumb-item active"><a href="contact.php">聯絡我們</a></li>
      </ol>
  </div>
  <section>
    <div class="container">
      <h1 class="title-sec title-sm text-main text-center mb-4">
        聯絡我們
      </h1>
      <div class="row">
        <div class="col-12 col-lg-10 mx-auto p-3 p-md-5">
          <form action="success.php">
            <div class="form-row">
              <div class="form-item col-md-6">
                  <label>中文姓名 <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="輸入姓名" required>
                  <span class="text-danger">姓名至少要輸入1個字</span>
              </div>
              <div class="form-item col-md-6">
                  <label>聯絡電話 <span class="text-danger">*</span></label>
                  <input type="tel" class="form-control" placeholder="輸入聯絡電話" required>
              </div>
              <div class="form-item col-12">
                  <label>Email <span class="text-danger">*</span></label>
                  <input type="email" class="form-control" placeholder="輸入 Email" required>
              </div>
              <div class="form-item col-12">
                  <label>主旨 <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="輸入主旨" required>
              </div>
              <div class="form-item col-12">
                  <label>訊息 <span class="text-danger">*</span></label>
                  <textarea rows="3" class="form-control" required></textarea>
                  <!-- 需加入字數上限 -->
                  <span class="text-danger">字數上限 xx 字</span>
              </div>
              <!-- TODO: 加入驗證碼 -->
              <div class="form-item col-12">
                <label>驗證碼<span class="text-danger">*</span></label>

                <div class="clearfix">
                  <input type="text" name="captcha" id="input-captcha" class="form-control float-left" style="width: calc(100% - 163px - 1rem);">
                  <img src="assets/img/jpg/capcha.jpg" alt="captcha" class="float-right" style="height: calc(1.5em + 0.75rem - 2px);">
                </div>

                <span class="text-danger">驗證碼輸入錯誤</span>

              </div>
              <!-- <div class="form-item col-6">
                  <label>驗證碼 <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" placeholder="" required style="width: calc(100% - 163px - 1rem);">
              </div> -->
            </div>
            <div class="text-right py-4"><button type="submit" class="btn btn-main">送出</button></div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>
<?php include('footer.php'); ?>