<?php include('header.php'); ?>
<div class="pagecont">
    <nav class="breadcrumbwrap">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">首頁</a></li>
                <li class="breadcrumb-item active"><a href="#">登出</a></li>
              </ol>
        </div>
    </nav>
    <div class="container pb-lg-5 pb-4">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
            <div class="box mt-lg-3 mt-0 text-center">
                <h1 class="title-sec title-sm text-main">會員登出</h1>
                <p>您已成功登出了您的帳號。 可以安全離開電腦了。</p>
                <a href="index.php" class="btn btn-main mt-3">回首頁</a>
            </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>