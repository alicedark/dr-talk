<?php include('header.php'); ?>
<div class="pagecont">
    <nav class="breadcrumbwrap">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item"><a href="#">錯誤</a></li>
            </ol>
        </div>
    </nav>
    <div class="container pb-5">
        <h1 class="title-sec title-sm text-main mb-5">ERROR</h1>
        <div class="row text-center">
            <div class="col-12">
                <p>Error Message</p>
                <a href="index.php" class="btn btn-main mt-3">回首頁</a>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>