<?php
    $home_videos = array(
        array(
            'date' => '2019-08-01',
            'thumb' => 'assets/img/demo-news.png',
            'title' => '文章標題1',
            'description' => '文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述。',
        ),
        array(
            'date' => '2019-07-15',
            'thumb' => 'assets/img/demo-news.png',
            'title' => '文章標題2',
            'description' => '文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述。',
        ),
        array(
            'date' => '2019-06-28',
            'thumb' => 'assets/img/demo-news.png',
            'title' => '文章標題3',
            'description' => '文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述。',
        ),
    );
    $news = array(
        array(
            'date' => '2019-08-01',
            'thumb' => 'assets/img/png/news_img01.png',
            'title' => '文章標題1',
            'description' => '文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述。',
        ),
        array(
            'date' => '2019-07-15',
            'thumb' => 'assets/img/png/news_img02.png',
            'title' => '文章標題2',
            'description' => '文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述。',
        ),
        array(
            'date' => '2019-06-28',
            'thumb' => 'assets/img/png/news_img03.png',
            'title' => '文章標題3',
            'description' => '文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述。',
        ),
        array(
            'date' => '2019-08-01',
            'thumb' => 'assets/img/png/news_img02.png',
            'title' => '文章標題1',
            'description' => '文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述。',
        ),
        array(
            'date' => '2019-07-15',
            'thumb' => 'assets/img/png/news_img01.png',
            'title' => '文章標題2',
            'description' => '文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述。',
        ),
    );
    $videos = array(
        array(
            'date' => '2019-08-01',
            'thumb' => 'assets/img/png/video_img01.png',
            'title' => '文章標題1',
            'description' => '文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述。',
        ),
        array(
            'date' => '2019-07-15',
            'thumb' => 'assets/img/png/video_img02.png',
            'title' => '文章標題2',
            'description' => '文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述。',
        ),
        array(
            'date' => '2019-06-28',
            'thumb' => 'assets/img/png/video_img03.png',
            'title' => '文章標題3',
            'description' => '文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述，文章3的簡述。',
        ),
        array(
            'date' => '2019-08-01',
            'thumb' => 'assets/img/png/video_img01.png',
            'title' => '文章標題1',
            'description' => '文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述，文章1的簡述。',
        ),
        array(
            'date' => '2019-07-15',
            'thumb' => 'assets/img/png/video_img01.png',
            'title' => '文章標題2',
            'description' => '文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述，文章2的簡述。',
        ),
    );

    $products = array(
        array(
            'image' => 'assets/img/home/product-01.jpg',
            'name' => '產品名稱產品名稱產品名稱產品名稱',
            'price' => 'NT$899',
            'special' => 'NT$998',
            'discount' => '-20%',
            'short_description' => '產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述。',
        ),
        array(
            'image' => 'assets/img/home/product-02.jpg',
            'name' => '產品名稱產品名稱產品名稱產品名稱',
            'price' => 'NT$899',
            'special' => 'NT$998',
            'discount' => '-20%',
            'short_description' => '產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述。',
        ),
        array(
            'image' => 'assets/img/home/product-01.jpg',
            'name' => '產品名稱產品名稱產品名稱產品名稱',
            'price' => 'NT$899',
            'special' => 'NT$998',
            'discount' => '-20%',
            'short_description' => '產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述。',
        ),
        array(
            'image' => 'assets/img/home/product-02.jpg',
            'name' => '產品名稱產品名稱產品名稱產品名稱',
            'price' => 'NT$899',
            'special' => 'NT$998',
            'discount' => '-20%',
            'short_description' => '產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述，產品簡述。',
        ),
    );
    $children = array(
        array(
            'name' => '第二層分類',
        ),
        array(
            'name' => '第二層分類',
        ),
    );
    $categories = array(
        array(
            'name' => '主分類',
            'children' => $children,
        ),
        array(
            'name' => '主分類',
            'children' => $children,
        ),
        array(
            'name' => '主分類',
            'children' => $children,
        ),
        array(
            'name' => '主分類',
            'children' => $children,
        ),
    );
?>