<div class="menu-list">
  <div>
    <?php $isOpen = false ?>
    <div class="text list-title <?= $isOpen ? "opened" : "" ?>">分類名稱1</div>
    <div class="list-content" style="<?= $isOpen ? "" : "display:none;" ?>">
      <a href="#" class="list-item active">子分類1</a>
      <a href="#" class="list-item">子分類2</a>
      <a href="#" class="list-item">子分類3</a>
    </div>
  </div>
  <div>
    <?php $isOpen = false ?>
    <div class="text list-title <?= $isOpen ? "opened" : "" ?>">分類名稱2</div>
    <div class="list-content" style="<?= $isOpen ? "" : "display:none;" ?>">
      <a href="#" class="list-item active">子分類1</a>
      <a href="#" class="list-item">子分類2</a>
      <a href="#" class="list-item">子分類3</a>
    </div>
  </div>
</div>