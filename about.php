<?php include("header.php") ?>
<div class="bg-light pagecont">
  <!-- PC: 1920 * 500 -->
  <div class="banner banner-page" style="background-image:url('assets/img/png/banner_img.png')"></div>
  <div class="container pt-3">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
      <li class="breadcrumb-item active"><a href="about.php">關於我們</a></li>
    </ol>
  </div>
  <section>
    <div class="container px-xl-3">
      <div class="col-sm-10 offset-sm-1 col-lg-8 offset-lg-2 px-xl-5">
        <h2 class="title-sec title-sm text-main text-center mb-4">
          什麼是 Dr. Talk？
        </h2>

        <h3 class="text text-muted text-center mb-3">再造嶄新醫學教育平台</h3>

        <div class="text text-center mb-4 mb-md-5">
          <p>
          Dr. Talk 是聚焦醫學領域學習的教育學院，以國際醫學教育委員會提出的六大核心能力為課程綱領，設計具備專業與實務的醫學進修內容，讓學員不論是在職涯或生活場域都能學以致用，提升專業與視野
          </p>
        </div>
      </div>
    </div>
  </section>

  <section class="bg-white home-section">
    <div class="container">
        <h2 class="title-sec title-sm text-main text-center mb-5">
          我可以如何在 Dr. Talk 進修？
        </h2>

      <div class="row">
        <div class="col-12 col-md-6 col-xl-4 mb-5">
          <div class="card step-card h-100 text-center">
            <span class="step-num">1</span>
            <h3 class="text text-lg mb-3">擁有個人專屬學習軌跡 </h3>
            <p class="text text-muted">
            不論是課程影片、文章專欄或是教材檔案，您都能依據各自專業科別與興趣找到對應又適合的學習內容，記錄學習歷程，累積職涯成長曲線 

            </p>
          </div>
        </div>
        <div class="col-12 col-md-6 col-xl-4 mb-5">
          <div class="card step-card h-100 text-center">
            <span class="step-num">2</span>
            <h3 class="text text-lg mb-3">增添閱覽新知便利性 </h3>
            <p class="text text-muted">
            平台介面設計清晰、操作簡便，整合多元線上學習類型與主題，同時滿足跨螢幕裝置的使用需求，隨時隨地皆可以觸及新知來源，輕鬆吸收線上學習內容 

            </p>
          </div>
        </div>
        <div class="col-12 col-md-6 col-xl-4 mb-5 mx-auto">
          <div class="card step-card h-100 text-center">
            <span class="step-num">3</span>
            <h3 class="text text-lg mb-3">加值職涯必修競爭力  </h3>
            <p class="text text-muted">
            參照國際醫學教育委員會架構方針，作為研發學習內容的整體方向，不僅兼具醫學專業知識與人文素養的概念思維，也與時俱進更新內容主題，持續注入活水新思維 

            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php include("footer.php") ?>