<?php include('header.php'); ?>
<div class="pagecont bg-light">
  <!-- PC: 1920 * 500 -->
  <div class="banner banner-page" style="background-image: url('assets/img/png/banner_img.png');"></div>
  <div class="container pt-3">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
      <li class="breadcrumb-item active"><a href="collect-file-list.php">檔案區</a></li>
    </ol>
  </div>

  <section class="file-section">
    <div class="container">
      <h1 class="title-sec text-main title-sm text-center mb-5">
        收藏檔案
      </h1>

      <div class="text text-muted">註: 收藏檔案 & 已觀看檔案 使用相同版型，上後台之後，方可動態切換</div>

<div class="row">
  <?php include('search-area.php'); ?>

  <div class="col-12 col-lg-3 mb-5">
    <?php include('collect-sidebar.php'); ?>
  </div>

  <div class="col-12 col-lg-9">
    <div class="row">
      <?php $has_collect = false ?>
      <? $i = 1?>
      <div class="col-12 d-sm-none">

      <?php for ($i = 0; $i < 6; $i++): ?>
        <table class="table table-file bg-white mb-4 w-100">
          <tbody>
            <tr>
              <th class="text-nowrap bg-light">上傳日期</th>
              <td>2020-10-10</td>
            </tr>
            <tr>
              <th class="text-nowrap bg-light">檔案名稱</th>
              <td>檔案標題檔案標題<?=$i?></td>
            </tr>
            <tr>
              <th class="text-nowrap bg-light">下載</th>
              <td>
                <button class="btn btn-icon">
                  <i class="fas fa-download"></i>
                </button>
                <span>100</span>
              </td>
            </tr>
            <tr>
              <th class="text-nowrap bg-light">喜愛</th>
              <td>
                <button class="btn btn-icon">
                  <i class="fas fa-heart"></i>
                </button>
                <span>200</span>
              </td>
            </tr>
            <tr>
              <th class="text-nowrap bg-light">加入收藏</th>
              <td>
                <button class="btn btn-icon">
                  <i class="fas fa-plus"></i>
                </button>
                <span>300</span>
              </td>
            </tr>
          </tbody>
        </table>
        <?php endfor; ?>
      </div>
      <!-- 平板/桌機板 table -->
      <div class="col-12 d-none d-sm-block">
        <table class="table table-file bg-white text-center">
          <thead class="bg-light">
            <tr>
              <th class="text-nowrap">上傳日期</th>
              <th class="text-nowrap">檔案名稱</th>
              <th class="text-nowrap">下載</th>
              <th class="text-nowrap">喜愛</th>
              <th class="text-nowrap">加入收藏清單</th>
            </tr>
          </thead>
          <tbody>
            <?php for ($i = 0; $i < 6; $i++): ?>

            <tr>
              <td>2020-10-10</td>
              <td class="text-center">檔案標題檔案標題檔案標題<?=$i?></td>
              <td>
                <button class="btn btn-icon">
                  <i class="fas fa-download"></i>
                </button>
                <span class="d-none d-lg-inline-block">100</span>
              </td>
              <td>
                <button class="btn btn-icon">
                  <i class="fas fa-heart"></i>
                </button>
                <span class="d-none d-lg-inline-block">200</span>
              </td>
              <td>
                <button class="btn btn-icon">
                  <i class="fas fa-plus"></i>
                </button>
                <span class="d-none d-lg-inline-block">300</span>
              </td>
            </tr>
            <?php endfor; ?>
          </tbody>
        </table>
      </div>

      <div class="w-100 text-center">
        <ul class="list-page mb-5">
          <li><a href="news.php"><i class="fas fa-angle-double-left"></i></a></li>
          <li><a href="news.php"><i class="fas fa-angle-left"></i></a></li>
          <li><a href="news.php" class="active">1</a></li>
          <li><a href="news.php">2</a></li>
          <li><a href="news.php">3</a></li>
          <li><a href="news.php"><i class="fas fa-angle-right"></i></a></li>
          <li><a href="news.php"><i class="fas fa-angle-double-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

    </div>
  </section>
</div>
<?php include('footer.php'); ?>