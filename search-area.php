  <!-- <form class="col-12 col-lg-5 offset-lg-7 col-xl-4 offset-xl-8 mb-4">
    <div class="form-inline">
      <div class="input-group mr-3" style="width: 60%;">
        <input type="text" class="form-control" placeholder="輸入關鍵字搜尋標題" name="search" value="" aria-label="Search" aria-describedby="Search" require="">
        <div class="input-group-append">
            <button id="search_btn" class="btn btn-main-dark" type="submit"><i class="fas fa-search" aria-hidden="true"></i></button>
        </div>
      </div>

    <select name="" id="" class="form-control mb-0" style="width: calc(40% - 1rem);">
      <option value="">請選擇排序</option>
      <option value="">最新</option>
      <option value="">最多瀏覽</option>
      <option value="">最多喜愛</option>
    </select>
    </div>
  </form> -->


  <form class="col-12 col-lg-5 offset-lg-7 col-xl-4 offset-xl-8 search-wrap form-inline mb-4">
    <!-- <a class="nav-link menu-title-level1" href="about.php"> -->
    <select name="" id="" class="js-select2 select form-control mr-3">
      <option value="">請選擇排序</option>
      <option value="">最新</option>
      <option value="">最多瀏覽</option>
      <option value="">最多喜愛</option>
    </select>

    <div class="input-group" style="width: calc(60% - 1rem);">
      <input type="text" class="form-control" placeholder="輸入關鍵字搜尋標題" name="search" value="" aria-label="Search" aria-describedby="Search" require="">
      <div class="input-group-append">
          <button id="search_btn" class="btn btn-main-dark" type="submit"><i class="fas fa-search" aria-hidden="true"></i></button>
      </div>
    </div>

    <!-- <div class="input-group">
      <input type="text" class="form-control" placeholder="請輸入關鍵字" name="search" value="" aria-label="Search" aria-describedby="Search" require="">
      <button class="input-group-append">
        <i class="fas fa-search" aria-hidden="true"></i>
      </button>
    </div> -->
  </form>