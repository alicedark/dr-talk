<!doctype html>
<html>

<head>
  <title>Dr.Talk</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta name="author" content="Codepulse" />

  <link href="assets/img/fav.png" rel="shortcut icon" type="image/png">

  <!--  Font family -->
  <link
    href="https://fonts.googleapis.com/css?family=Noto+Sans+TC:300,400,500,600|Noto+Sans:300,400,500,600&display=swap"
    rel="stylesheet" type="text/css">

  <!--  Plugins -->
  <link href="assets/css/animate.css" rel="stylesheet" type="text/css">
  <link href="assets/css/magnific-popup.css" rel="stylesheet" type="text/css">
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="assets/css/fontawesome.all.min.css" rel="stylesheet" type="text/css">
  <link href="assets/css/slick.css" rel="stylesheet" type="text/css">
  <link href="assets/css/select2-4.1.0.min.css" rel="stylesheet" type="text/css">

  <!--  Customized -->
  <link href="assets/css/common.css?modified=<?= time() ?>" rel="stylesheet" type="text/css">
</head>

<?php $active_link = false ?>
<?php include('data.php'); /*Demo用*/ ?>

<body>
  <div class="headerwrap">
    <div class="topbar py-0">
      <div class="container">
        <div class="text-right px-1">
          <a href="login.php">登入</a> | <a href="register-step1.php">註冊</a>

          <!-- <div class="btn-group btn-lang-group">
            <button class="btn lang-btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            English
            </button>
            <div class="lang-dropdown-menu dropdown-menu dropdown-menu-right" x-placement="bottom-end">
              <a class="dropdown-item" href="#">中文</a>
              <a class="dropdown-item" href="#">English</a>
              <a class="dropdown-item" href="#">日文</a>
            </div>
          </div> -->
        </div>
      </div>
    </div>

    <header class="">
      <div class="container">
        <nav class="navbar navbar-expand-lg p-0">
          <!-- empty item: make brand horizontally center -->
          <!-- <span></span> -->

          <a class="navbar-brand" href="index.php">
            <img src="assets/img/png/nav_logo.png">
          </a>
          <button class="navbar-toggler px-0 collapsed" type="button" data-toggle="collapse" data-target="#navbar">
            <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
          </button>

          <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav w-100 justify-content-end">

              <form class="nav-item search-wrap form-inline">
                <!-- <a class="nav-link menu-title-level1" href="about.php"> -->
                <select name="" id="" class="js-select2 select form-control">
                  <option value="">Options</option>
                  <option value="">Options</option>
                  <option value="">Options</option>
                  <option value="">Options</option>
                  <option value="">Options</option>
                </select>

                <div class="input-group">
                  <input type="text" class="form-control" placeholder="請輸入關鍵字" name="search" value="" aria-label="Search" aria-describedby="Search" require="">
                  <button class="input-group-append">
                    <i class="fas fa-search" aria-hidden="true"></i>
                  </button>
                </div>
              </form>
              <li class="nav-item">
                <!-- <a class="nav-link menu-title-level1" href="about.php"> -->
                <a class="nav-link menu-title-level1" href="about.php">
                  關於我們
                </a>
              </li>

              <li class="nav-item">
                <!-- <a class="nav-link menu-title-level1" href="news.php"> -->
                <a class="nav-link menu-title-level1" href="news.php">
                  最新消息
                </a>
              </li>

              <li class="nav-item">
                <!-- <a class="nav-link menu-title-level1" href="faq.php"> -->
                <a class="nav-link menu-title-level1" href="news.php">
                  文章區
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link menu-title-level1" href="video-list.php">
                  影片區
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link menu-title-level1" href="file-list.php">
                  檔案區
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link menu-title-level1" href="contact.php">
                  聯絡我們
                </a>
              </li>
            </ul>
          </div>
      </div>
      </nav>
  </div>
  </header>
  </div>