<?php include('header.php'); ?>
<div class="pagecont">
    <nav class="breadcrumbwrap">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item active"><a href="_login.php">註冊 (Step 1)</a></li>
              </ol>
        </div>
    </nav>
    <div class="container pb-lg-5 pb-4">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="box mt-lg-3 mt-0 shadow-card p-4">
                    <h1 class="title-sec title-sm text-main">註冊</h1>
                    <h2 class="subtitle-sec text-main mb-4">Step 1</h2>

                    <form action="_account.php" class="text-center">
                        <a href="register-step2.php" class="btn btn-main">
                            <i class="fab fa-line mr-2"></i> Line 快速註冊並綁定</a>
                        <hr class="my-5">

                        <p>已經有帳號了嗎? &nbsp; <a href="login.php">會員登入</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>