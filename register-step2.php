<?php include('header.php'); ?>
<div class="pagecont">
    <nav class="breadcrumbwrap">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">首頁</a></li>
                <li class="breadcrumb-item active"><a href="#">會員註冊</a></li>
            </ol>
        </div>
    </nav>
    <div class="container pb-lg-5 pb-4">
        <div class="row">
            <div class="col-12">
                <div class="box mt-lg-3 mt-0">
                    <h1 class="title-sec title-sm text-main">會員註冊</h1>
                    <h2 class="subtitle-sec text-main mb-5">Step 2</h1>
                    <form action="success.php">
                        <div class="row">
                            <div class="col-lg-6 offset-lg-3">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="item mb-4">
                                            <label>暱稱 <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" value="王曉明 (自動帶入Line 暱稱)" />
                                            <div class="text-danger">請填入暱稱</div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="item mb-4">
                                            <label>中文姓名 <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="item mb-4">
                                            <label>E-mail <span class="text-danger">*</span></label>
                                            <input class="form-control" type="email"/>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="item mb-4">
                                            <label>手機號碼 <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 offset-lg-3">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="item mb-4">
                                            <label>職業 <span class="text-danger">*</span></label>
                                            <select name="" id="" class="js-select2-single form-control">
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                                <option value="">Option 3</option>
                                                <option value="">Option 4</option>
                                                <option value="">Option 5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="item mb-4">
                                            <label>縣市 <span class="text-danger">*</span></label>
                                            <!-- TODO:縣市列表 -->
                                            <select name="" id="" class="js-select2-single form-control">
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                                <option value="">Option 3</option>
                                                <option value="">Option 4</option>
                                                <option value="">Option 5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="item mb-4">
                                            <label>執業單位類型 <span class="text-danger">*</span></label>
                                            <select name="" id="" class="js-select2-single form-control">
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                                <option value="">Option 3</option>
                                                <option value="">Option 4</option>
                                                <option value="">Option 5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="item mb-4">
                                            <label>執業單位名稱 <span class="text-danger">*</span></label>
                                            <select name="" id="" class="js-select2-single-search form-control">
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                                <option value="">Option 3</option>
                                                <option value="">Option 4</option>
                                                <option value="">Option 5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="item mb-4">
                                            <label>執業科別 <span class="text-danger">*</span></label>
                                            <!-- 科別請參照雲端「科別.xlsx」 -->
                                            <select name="" id="" class="js-select2-single-search form-control">
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                                <option value="">Option 3</option>
                                                <option value="">Option 4</option>
                                                <option value="">Option 5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 offset-lg-3">
                                <div class="item mb-4">
                                    <label>興趣 <span class="text-danger">*</span></label>
                                    <br>

                                    <select name="" id="" class="js-select2-multiple form-control" multiple>
                                        <option value="1">過敏免疫風濕科</option>
                                        <option value="2">麻醉科</option>
                                        <option value="3">乳房外科</option>
                                        <option value="4">心臟內科</option>
                                        <option value="5">神經外科</option>
                                    </select>

                                    <!-- 科別請參照雲端「科別.xlsx」 -->
                                    <!-- <label class="checkbox mr-3">
                                        <input class="form-control" type="checkbox">
                                        <span>過敏免疫風濕科</span>
                                    </label>

                                    <label class="checkbox mr-3">
                                        <input class="form-control" type="checkbox">
                                        <span>麻醉科</span>
                                    </label>

                                    <label class="checkbox mr-3">
                                        <input class="form-control" type="checkbox">
                                        <span>乳房外科</span>
                                    </label>

                                    <label class="checkbox mr-3">
                                        <input class="form-control" type="checkbox">
                                        <span>心臟內科</span>
                                    </label> -->
                                </div>
                            </div>
                            <div class="col-lg-6 offset-lg-3 mb-4">
                                <p class="text text-muted sm">
                                    1. 當您按下註冊按鈕時，即代表您已閱讀且同意本網站的<a href="info.php">服務條款</a>及<a href="privacy.php">隱私權政策</a>。
                                    <br><br>
                                    2. 此網站僅限醫療人員使用，經由您提供資料的註冊與點閱，您進入本網站將被視為您具有醫療人員身分的瀏覽與使用，如您非本網站所同意進入的醫療專業人員，請盡速離開本系統。非醫療人員使用本網站應自行承擔相關法律責任。
                                </p>
                            </div>
                        </div>

                        <div class="text-center">
                            <a class="btn btn-main" href="login.php">註冊</a>
                            <!-- <button type="submit" class="btn btn-main">註冊</button> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>

<script>
    $(".js-select2-single-search").select2({
        theme: "classic",
        width: "100%"
    });

    $(".js-select2-single").select2({
        theme: "classic",
        width: "100%",
        minimumResultsForSearch: Infinity
    });

    $(".js-select2-multiple").select2({
        theme: "classic",
        width: "100%"
    });
</script>