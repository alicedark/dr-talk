<?php include('header.php'); ?>
<div class="pagecont">
    <nav class="breadcrumbwrap">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item active"><a href="account.php">會員中心</a></li>
              </ol>
        </div>
    </nav>
    <div class="container pb-lg-5 pb-4">
        <div class="row">
          <div class="col-12 mb-4">
            <h1 class="title-sec title-sm mb-5">收藏清單</h1>
            <div class="text text-muted">註: 收藏清單 & 已觀看清單 使用相同版型，上後台之後，方可動態切換</div>
          </div>

          <div class="col-lg-3 account-sidebar">
            <div class="list-group">
              <a href="#" class="list-group-item list-group-item-action">
                <i class="fas fa-id-card"></i> 會員資料
              </a>

              <a href="collect-list.php" class="list-group-item list-group-item-action">
                <i class="fas fa-heart"></i> 收藏清單
              </a>

              <a href="collect-list.php" class="list-group-item list-group-item-action">
                <i class="fas fa-eye"></i> 已觀看清單
              </a>

              <a href="news.php" class="list-group-item list-group-item-action">
                <i class="fas fa-newspaper"></i> 文章區
              </a>

              <a href="video-list.php" class="list-group-item list-group-item-action">
                <i class="fas fa-video"></i> 影片區
              </a>

              <a href="file-list.php" class="list-group-item list-group-item-action">
                <i class="fas fa-file"></i> 檔案區
              </a>
            </div>
          </div>

            <div class="col-lg-9">
                <div>
                    <div class="row no-gutters member-func">
                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="collect-news.php">
                                    <i class="fas fa-newspaper"></i>
                                    <h4>收藏文章</h4>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="collect-video-list.php">
                                    <i class="fas fa-video"></i>
                                    <h4>收藏影片</h4>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="collect-file-list.php">
                                    <i class="fas fa-file"></i>
                                    <h4>收藏檔案</h4>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>


<div class="col-lg-8 offset-lg-2">