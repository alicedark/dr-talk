<?php include('header.php'); ?>
<div class="pagecont">
    <nav class="breadcrumbwrap">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item active"><a href="_login.php">會員登入</a></li>
              </ol>
        </div>
    </nav>
    <div class="container pb-lg-5 pb-4">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="box mt-lg-3 mt-0 shadow-card p-4">
                    <h1 class="title-sec title-sm text-main mb-5">會員登入</h1>
                    <form action="_account.php" class="text-center">
                        <a href="account.php" class="btn btn-main">
                            <i class="fab fa-line mr-2"></i> Line 快速登入</a>
                        <hr class="my-5">

                        <p>還沒有成為會員嗎? &nbsp; <a href="register-step1.php">會員註冊</a></p>
                    </form>
                    <!-- <hr class="my-5">
                    <h4 class="text-center mb-4">成為會員</h4>
                    <a href="register.php" class="btn btn-main-2 w-100">會員註冊</a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>