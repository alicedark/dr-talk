$(document).ready(function () {

  // init wow animation
  var wow = new WOW(
    {
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: true,
      live: true
    }
  )
  wow.init();
  // setup Menu Dropdown Style
  // JQuery select2
  // Header 和 最新消息 等頁面的搜尋區
  $('.search-wrap .js-select2').select2({
    width: '40%',
    theme: 'default',
    minimumResultsForSearch: Infinity
  })
  // submenu click function
  var hasBindSubmenuClick = false

  if ($(window).width() < 992 && !hasBindSubmenuClick) {
    $('.has_submenu>a').on('click', handleSubmenuClick)

    hasBindSubmenuClick = true
  }

  $(window).on('resize', function (e) {
    if ($(window).width() < 992 && !hasBindSubmenuClick) {
      $('.has_submenu>a').on('click', handleSubmenuClick)

      hasBindSubmenuClick = true
    } else if ($(window).width() >= 992 && hasBindSubmenuClick) {
      $('.has_submenu').children('.submenu').css({ "display": "" })
      $('.has_submenu>a').unbind('click', handleSubmenuClick)

      hasBindSubmenuClick = false
    }
  })

  function handleSubmenuClick (e) {
    e.preventDefault();

    $(this).siblings('.submenu').slideToggle();

    var $parent = $(this).closest('.has_submenu')
    $parent.siblings('.has_submenu').children('.submenu').slideUp()

  }

  function handleCategoryMenuClick () {
    $('.menu-list .list-title').on('click', function(e) {
      var toggleOpen = ($el) => {
        $el.addClass('opened');
        $el.siblings('.list-content').slideDown(200);
      }
      var toggleClose = ($el) => {
        $el.removeClass('opened');
        $el.siblings('.list-content').slideUp(200);
      }

      if ($(this).hasClass('opened')) {
        toggleClose($(this))
      } else {
        toggleOpen($(this))
      }

    });
  }

  function setupCategoryMenu () {
    // 初始化
    var el = $('.menu-list .list-title')

    var toggleOpen = ($el) => {
      $el.addClass('opened');
      $el.siblings('.list-content').show();
    }
    var toggleClose = ($el) => {
      $el.removeClass('opened');
      $el.siblings('.list-content').hide();
    }
    if (el.hasClass('opened')) {
      // 若 .list-title 為 opened，打開 .list-content
      toggleOpen(el)
    } else {
      toggleClose(el)
    }

  }

  // setupCategoryMenu()
  handleCategoryMenuClick()

  // toggleopen click function
  $('.toggleopen').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('opened');
    $(this).next().slideToggle();
  });

  // set page height
  var topbottomh = $('.headerwrap').height() + $('.footer').height();
  $('.pagecont').css('min-height', 'calc(100vh - ' + topbottomh + 'px)');

  // home slider setting
  $('.mainslider').slick({
    dots: false,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 4000,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    adaptiveHeight: true
  });

  // normal slider
  // $('.slider-col-3').slick({
  //   dots: true,
  //   arrows: false,
  //   autoplay: true,
  //   // autoplay: false,
  //   autoplaySpeed: 3000,
  //   infinite: true,
  //   speed: 200,
  //   slidesToShow: 4,
  //   adaptiveHeight: true,
  //   responsive: [
  //     {
  //       breakpoint: 768,
  //       settings: {
  //         slidesToShow: 2
  //       }
  //     }
  //     // {
  //     //   breakpoint: 480,
  //     //   settings: {
  //     //     slidesToShow: 1,
  //     //   }
  //     // }
  //   ]
  // });

  // product gallery slider setting
  var album = $('.gallery');
  var pager = $('.thumbs');
  var picPopup = $('.popup');

  album.slick({
    mobileFirst: true,
    autoplay: true,
    autoplaySpeed: 5000,
    fade: true,
    arrows: false,
    draggable: false,
    swipe: false,
    cssEase: 'cubic-bezier(0,.4,.4,1)',
    asNavFor: pager
  });

  pager.slick({
    mobileFirst: true,
    dots: false,
    arrows: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    swipeToSlide: true,
    focusOnSelect: true,
    asNavFor: album,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
        }
      }
    ]
  });

  // pictures popup
  picPopup.magnificPopup({
    type: 'image',
    tLoading: 'Loading ...',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    }
  });

  // scroll functions
  var menuh = $('header').height();
  $(window).on('scroll', function () {
    if ($(window).scrollTop() > menuh) {
      $('.headerwrap').addClass('fixed');
      $('.headerwrap').css('padding-top', menuh);
    } else {
      $('.headerwrap').removeClass('fixed');
      $('.headerwrap').css('padding-top', 0);
    }
    if ($(window).scrollTop() > 300) {
      $('.scrollToTop').fadeIn();
    } else {
      $('.scrollToTop').fadeOut();
    }
  });
  $('.scrollToTop').on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    return false;
  });
});
