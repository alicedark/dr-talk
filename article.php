<?php include('header.php'); ?>
<div class="pagecont border-top pt-3">
  <div class="container px-4">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
      <li class="breadcrumb-item"><a href="news.php">最新消息</a></li>
      <li class="breadcrumb-item active"><a href="article.php">文章標題</a></li>
    </ol>
  </div>
  <section>
    <div class="container px-4 news-section">
      <div class="row">
        <div class="col-lg-8 offset-lg-2">
          <div class="d-flex justify-content-between">
            <div>
              <!-- 標題 -->
              <h1 class="title-sec title-sm text-main text-left mb-2">文章標題</h1>
              <span class="date">2019-07-15</span>
            </div>

            <!-- 流量, 喜愛, 收藏 -->
            <div>
              <!-- 喜愛 -->
              <a class="js-action-love icon-wrap text-black text-hover-main" href="javascript:void(0);">
                <i class="far fa-heart" style="margin: 0 1px;"></i>&nbsp;喜愛：186
              </a>
              <br>
              <!-- 未收藏 -->
              <a class="js-action-collect icon-wrap text-black text-hover-main" href="javascript:void(0);">
                <i class="fas fa-plus" style="margin: 0 2px;"></i>&nbsp;收藏：186
              </a>
              <br>
              <a class="mr-1 js-view-count icon-wrap text-black text-hover-main" href="javascript:void(0);">
                <i class="fas fa-eye" style="margin: 0 0;"></i>&nbsp;觀看：555
              </a>
            </div>
          </div>

          <div class="mb-5 article-seo-img">
            <!-- <img src="assets/img/png/news_img01.png"> -->
          </div>
          <div class="mb-5 edit-area">

            <img src="assets/img/png/news_img01.png" style="width: 1000px;">

            <div class="youtube-embed-wrapper" style="position:relative;padding-bottom:56.25%;padding-top:30px;height:0;overflow:hidden"><iframe allow=";" allowfullscreen="" frameborder="0" height="1080" src="https://www.youtube.com/embed/hfySDsMW8BU" style="position:absolute;top:0;left:0;width:100%;height:100%" width="1920"></iframe></div>
            <br>
            <blockquote>影片僅供測試用途</blockquote>
            <h2>文章標題</h2>
              <p>文章內文</p>
            <h3>文章標題</h3>
            <h5>文章標題</h5>
            <div class="description mt-4 mb-4">
              <p>文章內文文章內文文章內文文章內文文章內文</p>
            <h4>文章標題</h4>
              <p>文章內文文章內文文章內文文章內文</p>
            <h6>文章標題</h6>
              <p>文章內文文章內文文章內文文章內文</p>
            </div>
            <ul>
              <li>項目內容項目內容</li>
              <li>項目內容項目內容</li>
              <li>項目內容項目內容</li>
            </ul>

            <ol>
              <li>項目內容項目內容</li>
              <li>項目內容項目內容</li>
              <li>項目內容項目內容</li>
            </ol>
          </div>

          <div>
            <!-- 只有影片內頁顯示此區 -->
            <p class="text text-muted mb-0" style="font-size: 12px;">(註:只有影片內頁顯示"附檔"區塊)</p>
            <h4 class="text mb-3">附檔</h4>
            <ul class="fa-ul" style="margin-left: 1.5em;">
              <li><a href="javascript::void(0);"><span class="fa-li"><i class="fas fa-download"></i></span>講義1</a></li>
              <li><a href="javascript::void(0);"><span class="fa-li"><i class="fas fa-download"></i></span>講義2</a></li>
              <li><a href="javascript::void(0);"><span class="fa-li"><i class="fas fa-download"></i></span>講義3</a></li>
              <li><a href="javascript::void(0);"><span class="fa-li"><i class="fas fa-download"></i></span>講義4</a></li>
            </ul>
          </div>

          <div class="author-area">
            <!-- 作者欄位：照片、名字、副標題、簡介 -->
            <div class="box-image author-photo" style="background-image: url('assets/img/png/news_img01.png');"></div>

            <div class="author-content">
              <div class="author-subtitle text sm">哈佛之後的人生</div>
              <h4 class="author-name text">Ming Wang 王曉明</h4>
              <p class="author-text text sm">簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介簡介</p>
            </div>
          </div>

          <hr>
          <div class="row no-gutters">
            <div class="col-4"><a href="news.php" class="textbtn"><i class="fas fa-angle-left"></i> 上一篇</a></div>
            <div class="col-4 text-center"><a href="news.php" class="textbtn">回列表</a></div>
            <div class="col-4 text-right"><a href="news.php" class="textbtn">下一篇 <i class="fas fa-angle-right"></i></a></div>
          </div>
          <hr>
        </div>
      </div>
    </div>
  </section>
</div>
<?php include('footer.php'); ?>