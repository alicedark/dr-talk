<?php include('header.php'); ?>
<div class="pagecont">
  <!-- Banner Slider -->
  <section class="slidersec">
    <div class="mainslider">
      <? for($i=1;$i<=3;$i++): ?>
      <div class="item">
          <div class="banner" style="background-image:url('assets/img/png/banner_img.png')">
              <div class="content pb-4">
              <!-- START -->
                  <!-- <div class="container" class="position-absolute" style="bottom: 0;"> -->
                  <div class="container">
                      <div class="row">
                        <div class="col-12 col-md-6">
                          <div class="card">
                            <div class="card-body">
                              <!-- 字數上限: 14 字(中文) -->
                              <h1>標題標題標題標題標題標題標題</h1>
                              <!-- 字數上限: 60 字(英文) -->
                              <!-- 字數上限: 32 字(中文) -->
                              <p>簡介文字簡介文字簡介文字簡介文字簡介文字簡介文字簡介文字文字簡介</p>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              <!-- END -->
              </div>
          </div>
      </div>
      <? endfor; ?>
    </div>
  </section>
  
  <!-- 登入後方可顯示此區塊 -->
  <section class="news-section bg-white">
    <div class="container">
      <h2 class="title-sec title-sm">
        精選影片
      </h2>
      <h3 class="subtitle-sec mb-5">Video</h3>
      <div class="row">
        <!-- 最多三篇 -->
        <?php foreach($home_videos as $key => $n): ?>
        <div class="col-12 col-md-6 col-xl-4 <?=($key === 2) ? "mx-auto" : "" ?>">
          <div class="card card-video mb-5">
            <a href="article.php" tabindex="0">
              <div class="box-img r-3-2" style="background-image:url('assets/img/png/video_img01.png');"></div>
            </a>
            <div class="card-body">
              <h4 class="title-card mb-1"><a href="article.php" class="stretched-link txt-l1" tabindex="0"><?=$n['title']?></a></h4>

              <p class="card-text txt-l2 mb-3 text-muted"><?=$n['description']?></p>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
        <div class="col-12 text-center">
          <a href="video-list.php" class="btn btn-main">了解更多</a>
        </div>
      </div>
    </div>
  </section>

  <section class="news-section home-section bg-light">
    <div class="container">
      <h2 class="title-sec title-sm text-center">
        精選文章
      </h2>
      <h3 class="subtitle-sec mb-5">News</h3>
      <!-- 文章樣式分為電腦和手機板 -->

      <!-- 電腦版, Pad版:卡片 -->

      <div class="row d-none d-lg-flex mb-5">
        <!-- 焦點文章卡片(1篇) -->
        <div class="col-12 col-sm-10 offset-sm-1 col-lg-6 offset-lg-0">
          <div class="card card-full card-article" style="height: calc(100% - 1rem);">
            <a href="article.php" tabindex="0">
              <div class="box-img r-3-2" style="background-image:url('assets/img/png/news_img01.png');"></div>
            </a>
            <div class="card-body">
              <h4 class="title-card mb-2">
                <a href="article.php" class="stretched-link txt-l2" tabindex="0">
                  管家皆經層層審核、一一面試，專業且備良民證，讓您放心託付。
                </a>
              </h4>

              <p class="card-text txt-l2 mb-3 text-muted">管家皆經層層審核、一一面試，專業且備良民證，讓您放心託付。公開價格、線上付款、開立發票，上市公司經營，安心免煩惱。</p>
            </div>
          </div>
        </div>
        <!-- 橫向文章卡片:最多三篇 -->
        <div class="col-12 col-sm-10 offset-sm-1 col-lg-6 offset-lg-0">
          <div class="row no-gutters">
          <?php for($i = 0;$i< 3; $i++): ?>
          <div class="home-text-card col-12 d-flex mb-3">
            <!-- 圖片 -->
            <div class="home-text-card-head">
            <div class="box-img r-3-2 rounded" style="background-image:url('assets/img/png/news_img01.png');"></div>
              <!-- <a href="article.php" tabindex="0">
              </a> -->
            </div>

            <!-- 文字 -->
            <div class="home-text-card-content d-flex align-items-center px-3">
              <div class="">
                <h4 class="title-card mb-2">
                  <a href="article.php" class="stretched-link" tabindex="0">
                    服務流程標準化，家電清洗皆具保固，重視服務品質
                  </a>
                </h4>

                <p class="card-text text-muted">
                  服務流程標準化，家電清洗皆具保固，重視服務品質貼心只為您服務流程標準化，家電清洗皆具保固。
                </p>
              </div>
            </div>
          </div>
          <?php endfor; ?>
          </div>
        </div>

      </div>

      <!-- 手機板:卡片 -->

      <div class="row d-lg-none">
        <!-- 最多四篇 -->
          <?php for($i = 0; $i < 4; $i++): ?>
        <div class="col-12 col-md-6 col-xl-4">
          <div class="card card-article mb-5">
            <a href="article.php" tabindex="0">
              <div class="box-img r-3-2" style="background-image:url('assets/img/png/news_img01.png');"></div>
            </a>
            <div class="card-body">
              <h4 class="title-card mb-1"><a href="article.php" class="stretched-link txt-l1" tabindex="0">管家皆經層層審核、一一面試，專業且備良民證，讓您放心託付。</a></h4>

              <p class="card-text txt-l2 mb-3 text-muted"><?=$n['description']?></p>
            </div>
          </div>
        </div>
        <?php endfor; ?>
    </div>

    <div class="row">
      <div class="col-12 text-center">
          <!-- 連到文章區 -->
          <a href="news.php" class="btn btn-main">了解更多</a>
      </div>
    </div>
  </section>

  <section class="home-section">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 d-flex align-items-center py-5 py-md-0">
          <div class="text-center px-3">
            <h3 class="title-sec text-main text-center font-weight-bold mb-4">什麼是 Dr. Talk？</h3>
            <p class="text-muted">
            Dr. Talk 是聚焦醫學領域學習的教育學院，以國際醫學教育委員會提出的六大核心能力為課程綱領，設計具備專業與實務的醫學進修內容，讓學員不論是在職涯或生活場域都能學以致用，提升專業與視野。
            </p>
            <a href="about.php" class="btn btn-main">了解更多</a>
          </div>
        </div>

        <div class="col-12 col-md-6 pt-5 pl-5 pl-md-2">
          <div class="box-img w-75 bordered" style="background-image:url('assets/img/png/news_img01.png');">
            <div class="box-img-oval-quote">
              <div class="oval-quote-inner font-weight-bold text">
                <!-- 每行字數上限 6 字 (最多兩行) -->
                再造嶄新<br>醫學教育平台
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

  <section class="bg-light home-section">
    <div class="container">
      <div class="row px-lg-5">
        <div class="col-12">
          <h3 class="title-sec title-sm text-center">我可以如何在 Dr. Talk 進修？</h3>
          <h3 class="subtitle-sec mb-5">service</h3>
        </div>
        <!-- <div class="col-12 col-sm-6 col-md-4 mb-4">
          <div class="card shadow-card h-100 p-4 py-lg-5 text-center border-0 rounded">
            <img class="mb-3" src="assets/img/svg/serve_icon01.svg" alt="" srcset="">
            <h3 class="text mb-3">多元化選擇</h3>
            <p class="text-muted">
              公開價格、線上付款、開立發票，上市公司經營，安心免煩惱。
            </p>
          </div>
        </div>

        <div class="col-12 col-sm-6 col-md-4 mb-4">
          <div class="card shadow-card h-100 p-4 py-lg-5 text-center border-0 rounded">
            <img class="mb-3" src="assets/img/svg/serve_icon05.svg" alt="" srcset="">
            <h3 class="text mb-3">嚴選優管家</h3>
            <p class="text-muted">
              公開價格、線上付款、開立發票，上市公司經營，安心免煩惱。
            </p>
          </div>
        </div> -->

        <div class="col-12 col-md-6 col-xl-4 mb-4">
          <div class="card shadow-card h-100 p-4 py-lg-5 text-center border-0 rounded">
            <img class="mb-3" src="assets/img/svg/serve_icon03.svg" alt="" srcset="">
            <h3 class="text mb-3">擁有個人專屬學習軌跡</h3>
            <p class="text-muted">
              不論是課程影片、文章專欄或是教材檔案，您都能依據各自專業科別與興趣找到對應又適合的學習內容，記錄學習歷程，累積職涯成長曲線。
            </p>
          </div>
        </div>

        <div class="col-12 col-md-6 col-xl-4 mb-4">
          <div class="card shadow-card h-100 p-4 py-lg-5 text-center border-0 rounded">
            <img class="mb-3" src="assets/img/svg/serve_icon04.svg" alt="" srcset="">
            <h3 class="text mb-3">增添閱覽新知便利性</h3>
            <p class="text-muted">
              平台介面設計清晰、操作簡便，整合多元線上學習類型與主題，同時滿足跨螢幕裝置的使用需求，隨時隨地皆可以觸及新知來源，輕鬆吸收線上學習內容 
            </p>
          </div>
        </div>

        <!-- 若為奇數個: 最後一個加上 mx-auto mx-lg-0 -->
        <div class="col-12 col-md-6 col-xl-4 mb-4 mx-auto mx-xl-0">
          <div class="card shadow-card h-100 p-4 py-lg-5 text-center border-0 rounded">
            <img class="mb-3" src="assets/img/svg/serve_icon02.svg" alt="" srcset="">
            <h3 class="text mb-3">加值職涯必修競爭力</h3>
            <p class="text-muted">
              參照國際醫學教育委員會架構方針，作為研發學習內容的整體方向，不僅兼具醫學專業知識與人文素養的概念思維，也與時俱進更新內容主題，持續注入活水新思維。
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="bg-white py-5 px-md-4">
    <div class="container py-4 px-md-5">
      <h3 class="title-sec title-sm text-center">
        合作夥伴
      </h3>
      <h3 class="subtitle-sec mb-5">service</h3>
      <div class="partner-slide px-3">

        <div class="item">
          <div class="box-img r-16-9 bgcover" style="background-image: url('assets/img/png/partner_logo01.png');"></div>
        </div>
        <div class="item">
          <div class="box-img r-16-9 bgcover" style="background-image: url('assets/img/png/partner_logo02.png');"></div>
        </div>
        <div class="item">
          <div class="box-img r-16-9 bgcover" style="background-image: url('assets/img/png/partner_logo03.png');"></div>
        </div>
        <div class="item">
          <div class="box-img r-16-9 bgcover" style="background-image: url('assets/img/png/partner_logo04.png');"></div>
        </div>
        <div class="item">
          <div class="box-img r-16-9 bgcover" style="background-image: url('assets/img/png/partner_logo05.png');"></div>
        </div>
        <div class="item">
          <div class="box-img r-16-9 bgcover" style="background-image: url('assets/img/png/partner_logo01.png');"></div>
        </div>
        <div class="item">
          <div class="box-img r-16-9 bgcover" style="background-image: url('assets/img/png/partner_logo02.png');"></div>
        </div>
        <div class="item">
          <div class="box-img r-16-9 bgcover" style="background-image: url('assets/img/png/partner_logo03.png');"></div>
        </div>
        <div class="item">
          <div class="box-img r-16-9 bgcover" style="background-image: url('assets/img/png/partner_logo04.png');"></div>
        </div>
        <div class="item">
          <div class="box-img r-16-9 bgcover" style="background-image: url('assets/img/png/partner_logo05.png');"></div>
        </div>
      </div>
    </div>
  </section>

</div>
<?php include('footer.php'); ?>
<script>
  $(document).ready(function () {
    // normal slider
    $('.partner-slide').slick({
      dots: false,
      arrows: true,
      autoplay: true,
      // autoplay: false,
      autoplaySpeed: 3000,
      infinite: true,
      speed: 200,
      slidesToShow: 5,
      adaptiveHeight: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 3,
          }
        }
      ]
    });
  })
</script>