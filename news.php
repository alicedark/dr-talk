<?php include('header.php'); ?>
<div class="pagecont news-section bg-light">
  <!-- PC: 1920 * 500 -->
  <div class="banner banner-page" style="background-image: url('assets/img/png/banner_img.png');"></div>
  <div class="container pt-3">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
      <li class="breadcrumb-item active"><a href="news.php">最新消息</a></li>
    </ol>
  </div>

  <section>
    <div class="container">
      <h1 class="title-sec text-main title-sm text-center mb-5">
        最新消息
      </h1>
      <div class="text text-muted">(註:最新消息 & 文章區 共用版型，上後台之後，方可動態切換)</div>

<div class="row">
  <?php include('search-area.php'); ?>

  <div class="col-12 col-lg-3 mb-5">
    <?php include('sidebar.php') ?>
  </div>

  <div class="col-12 col-lg-9">
    <div class="row">
      <?php $has_collect = false ?>
      <?php $has_love = false ?>
      <?php foreach($videos as $n): ?>
        <div class="col-12 col-sm-6 col-lg-12">
          <div class="card card-article no-shadow mb-5">
            <a href="article.php" tabindex="0">
              <div class="box-img r-3-2" style="background-image:url('<?=$n['thumb']?>');"></div>
            </a>
            <div class="card-body">
              <a href="article.php">
                <h4 class="title-card mb-1 txt-l1"><?=$n['title']?></h4>

                <p class="card-text txt-l2 mb-3 text-muted">
                    <?=$n['description']?>
                </p>
              </a>

              <div class="d-flex justify-content-between">
                <div>
                    <a class="text-black text-hover-main mr-1 js-view-count icon-wrap" href="javascript:void(0);">
                      <i class="fas fa-eye"></i>
                      8555
                    </a>

                  <?php if ($has_love) { ?>
                  <!-- 已收藏 -->
                  <a class="text-black text-hover-main js-action-dislove icon-wrap" href="javascript:void(0);">
                    <i class="fas fa-heart"></i>
                    186
                  </a>
                  <?php } else { ?>
                  <!-- 未收藏 -->
                  <a class="text-black text-hover-main js-action-love icon-wrap" href="javascript:void(0);">
                    <i class="far fa-heart"></i>
                    186
                  </a>
                  <?php }; ?>
                </div>

                <div><span class="date"><?=$n['date']?></span></div>
              </div>
            </div>
          </div>
        </div>
        <?php endforeach; ?>

        <div class="w-100 text-center">
          <ul class="list-page mb-5">
            <li><a href="news.php"><i class="fas fa-angle-double-left"></i></a></li>
            <li><a href="news.php"><i class="fas fa-angle-left"></i></a></li>
            <li><a href="news.php" class="active">1</a></li>
            <li><a href="news.php">2</a></li>
            <li><a href="news.php">3</a></li>
            <li><a href="news.php"><i class="fas fa-angle-right"></i></a></li>
            <li><a href="news.php"><i class="fas fa-angle-double-right"></i></a></li>
          </ul>
        </div>
    </div>
  </div>
</div>

    </div>
  </section>
</div>
<?php include('footer.php'); ?>