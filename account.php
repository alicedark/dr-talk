<?php include('header.php'); ?>
<div class="pagecont">
    <nav class="breadcrumbwrap">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">首頁</a></li>
                <li class="breadcrumb-item active"><a href="account.php">會員中心</a></li>
              </ol>
        </div>
    </nav>
    <div class="container pb-lg-5 pb-4">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="mt-lg-3 mt-0">
                    <h1 class="title-sec title-sm mb-5">會員中心</h1>
                    <div class="row no-gutters member-func">
                        <div class="col-12 text-right mb-5 px-5">
                            <a href="_logout.php">
                                <i class="fas fa-sign-out-alt"></i> 會員登出
                            </a>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="edit.php">
                                    <i class="fas fa-id-card"></i>
                                    <h4>會員資料</h4>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="collect-list.php">
                                    <i class="fas fa-heart"></i>
                                    <h4>收藏清單</h4>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="_point.php">
                                    <i class="fas fa-eye"></i>
                                    <h4>已觀看清單</h4>
                                </a>
                            </div>
                        </div>

                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="news.php">
                                    <i class="fas fa-newspaper"></i>
                                    <h4>文章區</h4>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="video-list.php">
                                    <i class="fas fa-video"></i>
                                    <h4>影片區</h4>
                                </a>
                            </div>
                        </div>
                        <div class="col-6 col-lg-4">
                            <div class="card">
                                <a href="file-list.php">
                                    <i class="fas fa-file"></i>
                                    <h4>檔案區</h4>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>